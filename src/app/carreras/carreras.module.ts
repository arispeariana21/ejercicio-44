import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarrerasRoutingModule } from './carreras-routing.module';
import { MedicinaComponent } from './pages/medicina/medicina.component';
import { SistemasComponent } from './pages/sistemas/sistemas.component';
import { ArquitecturaComponent } from './pages/arquitectura/arquitectura.component';


@NgModule({
  declarations: [
    MedicinaComponent,
    SistemasComponent,
    ArquitecturaComponent
  ],
  imports: [
    CommonModule,
    CarrerasRoutingModule
  ]
})
export class CarrerasModule { }
