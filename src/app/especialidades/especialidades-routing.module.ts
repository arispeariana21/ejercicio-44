import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FrontendComponent } from './pages/frontend/frontend.component';
import { BackendComponent } from './pages/backend/backend.component';
import { FullstackComponent } from './pages/fullstack/fullstack.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'frontend', component: FrontendComponent },
      { path: 'backend', component: BackendComponent },
      { path: 'fullstack', component: FullstackComponent },
      { path: '**', redirectTo: 'frontend' }
      
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class EspecialidadesRoutingModule { }
